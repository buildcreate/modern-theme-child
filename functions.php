<?php

	// enqueue child styles
	add_action('wp_enqueue_scripts', 'child_theme_enqueue_styles');
	function child_theme_enqueue_styles(){
	    wp_enqueue_style('child-style', get_stylesheet_directory_uri() . '/style.css');
	}

	// custom wysiwyg color palettes
	function tiny_mce_colors($init){
		$colors = array(
			'ffffff', 'white',
			'333333', 'dark grey'
		); 
		$color_string = implode('","', $colors);
		$init['textcolor_map'] = '["'.$color_string.'"]';
		return $init;
	}
	add_filter('tiny_mce_before_init', 'tiny_mce_colors', 99);
	
	// custom ACF Color palette
	function acf_custom_colors(){
		?>
		<script type="text/javascript">
			jQuery(document).ready(function($){
				if(typeof acf !== 'undefined'){
					// custom colors
					var palette = ['#ffffff','#333333'];

					// when initially loaded find existing colorpickers and set the palette
					acf.add_action('load', function(){
						$('input.wp-color-picker').each(function() {
							$(this).iris('option', 'palettes', palette);
						});
					});

					// if appended element only modify the new element's palette
					acf.add_action('append', function(el) {
						$(el).find('input.wp-color-picker').iris('option', 'palettes', palette);
					});
				}
			});
		</script>
		<?php
	}
	add_action('admin_print_scripts', 'acf_custom_colors', 99);
?>